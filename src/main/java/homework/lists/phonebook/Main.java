package homework.lists.phonebook;

public class Main {
    public static void main(String[] args) {
        PhoneBook phoneBook = new PhoneBook();
        phoneBook.add("Oleh","+380955745***");
        phoneBook.add("Valya","+380997844***");
        phoneBook.add("Oleh", "+380502113***");

        System.out.println("Поиск по имени Олег: " + phoneBook.find("Oleh").toString());
        System.out.println("Поиск по имени Валя: " + phoneBook.find("Valya").toString());

        System.out.println("Поиск по всем именам Олег:");
        phoneBook.findAll("Oleh").forEach(System.out::println);
    }
}
