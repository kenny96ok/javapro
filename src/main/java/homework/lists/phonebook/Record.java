package homework.lists.phonebook;

public record Record(String name, String number) {
    @Override
    public String toString() {
        return "Record{" +
                "name='" + name + '\'' +
                ", number='" + number + '\'' +
                '}';
    }
}
