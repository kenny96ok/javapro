package homework.lists;

public record CountOfWord(String word, int count) {
}
