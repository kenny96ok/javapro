package homework.lists;

import java.util.*;

public class ListFunctions {
    public static int countOccurance(List<String> list, String occuranceString) {
        int count = 0;
        for (String item : list) {
            if (item.compareTo(occuranceString) == 0)
                count++;
        }
        return count;
    }

    public static List<Integer> toList(int[] array) {
        List<Integer> result = new ArrayList<>(array.length);
        for (int i : array) {
            result.add(i);
        }
        return result;
    }

    public static List<Integer> findUnique(List<Integer> source) {
        List<Integer> unique = new ArrayList<>();
        for (int i = 0; i < source.size(); i++) {
            if (source.lastIndexOf(source.get(i)) == i && source.indexOf(source.get(i)) == i)
                unique.add(source.get(i));
        }
        return unique;
    }

    public static void calcOccurance(List<String> words) {
        Map<String, Integer> result = new HashMap<>();
        for (int i = 0; i < words.size(); i++) {
            if (result.containsKey(words.get(i)))
                result.put(words.get(i), result.get(words.get(i)) + 1);
            else
                result.put(words.get(i), 1);
        }
        result.forEach((word, count) -> System.out.println(word + ": " + count));
    }

    public static List<CountOfWord> findOccurance(List<String> words) {
        Map<String, Integer> wordsAndCounts = new HashMap<>();
        for (int i = 0; i < words.size(); i++) {
            if (wordsAndCounts.containsKey(words.get(i)))
                wordsAndCounts.put(words.get(i), wordsAndCounts.get(words.get(i)) + 1);
            else
                wordsAndCounts.put(words.get(i), 1);
        }

        List<CountOfWord> result = new ArrayList<>();
        wordsAndCounts.forEach((word, count) -> result.add(new CountOfWord(word, count)));
        return result;
    }

    public static void addFirst(LinkedList<String> list, String newLastString) {
        list.addLast(newLastString);
    }

    public static void addLast(LinkedList<String> list, String newFirstString) {
        list.addFirst(newFirstString);
    }

    public static LinkedList<Integer> join(LinkedList<Integer> first, LinkedList<Integer> second) {
        LinkedList<Integer> result = new LinkedList<>(first);
        result.addAll(second);
        return result;
    }

    public static LinkedList<String> shuffle(LinkedList<String> source) {
        LinkedList<String> result = new LinkedList<>(source);
        for (int i = 0, first, second; i < source.size(); i++) {
            first = new Random().nextInt(source.size());
            second = new Random().nextInt(source.size());
            String temp = result.get(first);
            result.set(first, result.get(second));
            result.set((second), temp);
        }
        return result;
    }

    public static LinkedList<Integer> intersect(LinkedList<Integer> first, LinkedList<Integer> second) {
        LinkedList<Integer> result = new LinkedList<>();
        first.forEach(f -> second.forEach(s -> {
            if (f.equals(s))
                result.add(f);
        }));
        return result;
    }
}
