package homework.lists.coffee.order;

import java.util.*;

public class CoffeeOrderBoard {
    private LinkedList<Order> orders = new LinkedList<>();
    private int lastNumberOrder = 1;

    public void add(String name) {
        orders.offer(new Order(lastNumberOrder++, name));
    }

    public Order deliver() {
        System.out.println("Заказ №" + orders.element().orderNumber() + " выдан!");
        return orders.poll();
    }

    public Order deliver(int orderNumber) {
        for (int i = 0; i < orders.size(); i++) {
            if (orders.get(i).orderNumber() == orderNumber) {
                System.out.println("Заказ №" + orders.get(i).orderNumber() + " выдан!");
                return orders.remove(i);
            }
        }
        return null;
    }

    public void draw() {
        System.out.println("=============\n   Num|  Name");
        orders.forEach(order -> System.out.printf("%6d|%6s\n", order.orderNumber(), order.name()));
    }
}
