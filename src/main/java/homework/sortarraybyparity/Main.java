package homework.sortarraybyparity;

public class Main {
    public static void main(String[] args) {
        int[] array = {3, 1, 2, 4};
        printIntegerArray(array);
        printIntegerArray(sortArrayByParity(array));
    }

    public static int[] sortArrayByParity(int[] source) {
        if (source.length == 0) return source;
        int[] dest = new int[source.length];
        for (int i = 0, j = 0, k = 1; i < source.length; i++) {
            if (source[i] % 2 == 0)
                dest[j++] = source[i];
            else
                dest[source.length - k++] = source[i];
        }
        return dest;
    }

    public static void printIntegerArray(int[] array) {
        for (int elem : array) {
            System.out.print(elem + " ");
        }
        System.out.println();
    }
}
