package homework.animal;

public class Animal {
    private static int animalCount;
    private String name;

    public Animal(String name) {
        this.name = name;
        animalCount++;
    }

    public void eat() {
        System.out.println(name + " кушает!");
    }

    public String getName() {
        return name;
    }

    public static int getAnimalCount() {
        return animalCount;
    }
}
