package homework.animal;

public interface Swimmable {
    void swim(int meters);
}
