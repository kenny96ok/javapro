package homework.animal;

public interface Runnable {
    void run(int meters);
}
