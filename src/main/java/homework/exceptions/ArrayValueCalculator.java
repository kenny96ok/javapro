package homework.exceptions;

public class ArrayValueCalculator {

    public static int doCalc(String[][] array) {
        if (array.length != 4)
            throw new MyArraySizeException("Number of array rows is not 4!");
        for (int i = 0; i < array.length; i++) {
            if (array[i].length != 4)
                throw new MyArraySizeException("The number of elements in row number " + (i + 1) + " is not equal 4.");
        }

        int result = 0;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                try {
                    result += Integer.parseInt(array[i][j]);
                } catch (NumberFormatException ex) {
                    throw new MyArrayDataException("In cell[" + (i + 1) + "][" + (j + 1) + "] not a Number!", ex);
                }
            }
        }
        return result;
    }
}
