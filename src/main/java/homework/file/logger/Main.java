package homework.file.logger;

import static homework.file.logger.LoggingLevel.*;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        FileLogger logger = new FileLogger(DEBUG, 8192, "[CURRENT_TIME] [LEVEL] Сообщение: [MESSAGE]");
        logger.info("FileLogger logger has started!");
        for(long millis = System.currentTimeMillis();true;) {
            if((System.currentTimeMillis()-millis)>=4000) {
                logger.info("Current program working");
            }
            if((System.currentTimeMillis()-millis)>=5000) {
                logger.debug("Main process throws InterruptedException");
                logger.warning("Thrown Exception");
                millis = System.currentTimeMillis();
            }
            Thread.sleep(100);
        }
    }
}
