package homework.file.logger;

public enum LoggingLevel {
    INFO, DEBUG, WARNING
}
