package homework.file.navigator;

import java.nio.file.Path;
import java.util.*;

public class FileNavigator {
    private Map<String, List<FileData>> paths = new HashMap<>();

    public void add(String path, FileData file) {
        if (path.compareTo(file.getPath()) != 0) {
            System.out.println("Путь: " + path + "\n" + file);
            return;
        }
        if (paths.containsKey(path))
            paths.get(path).add(file);
        else {
            paths.put(path, new ArrayList<>());
            paths.get(path).add(file);
        }
    }

    public List<FileData> find(String path) {
        return paths.get(Path.of(path).toString());
    }

    public List<FileData> filterBySize(long sizeInBytes) {
        List<FileData> result = new ArrayList<>();
        for (List<FileData> value : paths.values()) {
            result.addAll(value.stream().filter(file -> file.getSizeInBytes() <= sizeInBytes).toList());
        }
        return result;
    }

    public void remove(String path) {
        paths.remove(Path.of(path).toString());
    }

    public List<FileData> sortBySize() {
        List<FileData> result = new ArrayList<FileData>();
        paths.values().forEach(result::addAll);
        return result.stream().sorted(Comparator.comparingLong(FileData::getSizeInBytes)).toList();
    }
}
