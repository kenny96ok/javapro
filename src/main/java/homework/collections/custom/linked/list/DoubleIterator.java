package homework.collections.custom.linked.list;

import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;

public class DoubleIterator<E> implements ListIterator<E> {
    private final DoubleLinkedList<E> list;
    private int prevCount = 0;
    private int count = 0;

    public DoubleIterator(DoubleLinkedList<E> list) {
        this.list = list;
    }

    @Override
    public boolean hasNext() {
        return count < list.size();
    }

    @Override
    public E next() {
        if (!hasNext())
            throw new NoSuchElementException();
        prevCount = count;
        return list.get(count++);
    }

    @Override
    public boolean hasPrevious() {
        return count > 0;
    }

    @Override
    public E previous() {
        if (!hasPrevious())
            throw new NoSuchElementException();
        prevCount = count - 1;
        return list.get(--count);
    }

    @Override
    public int nextIndex() {
        return (count + 1 == list.size()) ? list.size() : count + 1;
    }

    @Override
    public int previousIndex() {
        return (count == 0) ? -1 : count - 1;
    }

    @Override
    public void remove() {
        list.remove(Objects.checkIndex(prevCount, list.size()));
        if(count == -1)
            count = prevCount = 0;
        else
            count = prevCount;
    }

    @Override
    public void set(E e) {
        list.set(prevCount, e);
    }

    @Override
    public void add(E e) {
        list.add(e);
    }
}
