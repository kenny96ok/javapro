package homework.collections.custom.linked.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SinglyIterator<E> implements Iterator<E> {
    private final SinglyLinkedList<E> list;
    private int count = 0;

    public SinglyIterator(SinglyLinkedList<E> list) {
        this.list = list;
    }

    @Override
    public boolean hasNext() {
        return count < list.size();
    }

    @Override
    public E next() {
        if (!hasNext())
            throw new NoSuchElementException();
        return list.get(count++);
    }
}
