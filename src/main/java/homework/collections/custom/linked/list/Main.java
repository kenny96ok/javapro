package homework.collections.custom.linked.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Main {
    public static void main(String[] args) {
//        SinglyLinkedList<Integer> digits = new SinglyLinkedList<>();
//
//        digits.add(1);
//        digits.add(5);
//        System.out.println("First: " + digits.get(0));
//        System.out.println("Second: " + digits.get(1));
//
//        digits.add(8);
//        System.out.println("Third: " + digits.get(2));
//
//        System.out.println("Size: " + digits.size());

//        digits.remove(digits.size() - 1);
//        digits.remove(digits.size() / 2);
//        digits.remove(0);
//        System.out.println("Size: " + digits.size());
//
//        System.out.println("First: " + digits.get(0));
//        System.out.println("Second: " + digits.get(1));

//        SinglyIterator<Integer> iterator = digits.iterator();
//        while (iterator.hasNext())
//            System.out.print(iterator.next() + " ");
//        System.out.println();

        DoubleLinkedList<Integer> digits2 = new DoubleLinkedList<>();
        for (int i = 0; i < 3; i++) {
            digits2.add(i+1);
        }
        System.out.println(digits2.size());
        digits2.removeLast();
        System.out.println(digits2.size());
        digits2.removeFirst();
        System.out.println(digits2.size());
        digits2.remove(0);
        System.out.println(digits2.size());

        for (int i = 0; i < 10; i++) {
            digits2.add(i+1);
            System.out.print(digits2.get(i) + " ");
        }
        System.out.println();
        System.out.println(digits2.remove(9));
        System.out.println(digits2.size());


        System.out.println(digits2.getFirst().getValue());
        System.out.println(digits2.getLast().getValue());

        DoubleIterator<Integer> doubleIterator = digits2.iterator();
        doubleIterator.add(10);

        while(doubleIterator.hasNext())
            System.out.print(doubleIterator.next() + " ");
        System.out.println();

        doubleIterator.remove();

        while(doubleIterator.hasPrevious())
            System.out.print(doubleIterator.previous() + " ");
        System.out.println();

        doubleIterator.remove();

        while(doubleIterator.hasNext())
            System.out.print(doubleIterator.next() + " ");
        System.out.println();
    }
}
