package homework.worker;

public class Employee {
    private String fullName;
    private String position;
    private String email;
    private String number;
    private int age;

    public Employee(String fullName, String position, String email, String number, int age) {
        this.fullName = fullName;
        this.position = position;
        this.email = email;
        this.number = number;
        this.age = age;
    }
}