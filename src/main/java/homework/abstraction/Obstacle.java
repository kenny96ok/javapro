package homework.abstraction;

public abstract class Obstacle {
    private double size;

    public Obstacle(double size) {
        this.size = size;
    }

    public double getSize() {
        return size;
    }

    public abstract <T extends Participant> boolean overcome(T participant);
}
