package homework.abstraction;

public abstract class Participant {
    private String name;
    private String className = "Участник";
    private int obstacleHeight;
    private int obstacleLength;

    public Participant(String className, String name) {
        this.name = name;
        this.className = className;
    }

    public String getName() {
        return name;
    }

    public String getClassName() {
        return className;
    }

    public int getObstacleHeight() {
        return obstacleHeight;
    }

    public int getObstacleLength() {
        return obstacleLength;
    }

    public void setObstacleHeight(int obstacleHeight) {
        this.obstacleHeight = obstacleHeight;
    }

    public void setObstacleLength(int obstacleLength) {
        this.obstacleLength = obstacleLength;
    }

    public abstract void run();

    public abstract void jump();
}
