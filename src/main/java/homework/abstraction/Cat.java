package homework.abstraction;

public class Cat extends Participant {
    public Cat(String name, int obstacleHeight, int obstacleLength) {
        super("Кот", name);
        setObstacleHeight(obstacleHeight);
        setObstacleLength(obstacleLength);
    }

    @Override
    public void run() {
        System.out.println("Кот " + getName() + " побежал!");
    }

    @Override
    public void jump() {
        System.out.println("Кот " + getName() + " прыгнул!");
    }
}
