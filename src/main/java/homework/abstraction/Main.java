package homework.abstraction;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Participant> participants= new ArrayList<>();
        participants.add(new Human("Олег", 5,200));
        participants.add(new Cat("Борис", 15, 100));
        participants.add(new Robot("T-800", 20, 300));

        List<Obstacle> obstacles = new ArrayList<>();
        obstacles.add(new Treadmill(150));
        obstacles.add(new Wall(10));

        participants.forEach(participant -> {
            for (Obstacle obstacle : obstacles)
                if (!obstacle.overcome(participant)) {
                    System.out.println(participant.getClassName() + " " +
                            participant.getName() + " выбывает из олимпиады!");
                    break;
                }
            });
    }
}
