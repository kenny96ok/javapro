package homework.abstraction;

public class Robot extends Participant {
    public Robot(String name, int obstacleHeight, int obstacleLength) {
        super("Робот", name);
        setObstacleHeight(obstacleHeight);
        setObstacleLength(obstacleLength);
    }

    @Override
    public void run() {
        System.out.println("Робот " + getName() + " побежал!");
    }

    @Override
    public void jump() {
        System.out.println("Робот " + getName() + " прыгнул!");
    }
}
