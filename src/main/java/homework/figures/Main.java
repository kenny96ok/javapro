package homework.figures;

public class Main {
    public static void main(String[] args) {
        Figure figures[] = new Figure[3];
        figures[0] = new Circle(3);
        System.out.println("Площадь круга: " + figures[0].square());
        figures[1] = new Triangle(3,3,3);
        System.out.println("Площадь треугольника: " + figures[1].square());
        figures[2] = new Square(3);
        System.out.println("Площадь квадрата: " + figures[2].square());
        System.out.println("Сумма квадратов: " + sumFigureSquares(figures));
    }

    public static double sumFigureSquares(Figure[] figures) {
        double result = 0;
        for (int i = 0; i < figures.length; i++) {
            result += figures[i].square();
        }
        return result;
    }
}
