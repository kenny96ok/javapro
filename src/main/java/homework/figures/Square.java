package homework.figures;

public class Square implements Figure {
    private double a;

    public Square(double a) {
        this.a = a;
    }

    @Override
    public double square() {
        return Math.pow(a,2);
    }
}
