package homework.figures;

public class Triangle implements Figure{
    private double[] sides = new double[3];

    public Triangle(double a, double b, double c) {
        this.sides[0] = a;
        this.sides[1] = b;
        this.sides[2] = c;
    }

    @Override
    public double square() {
        double p = (this.sides[0]+this.sides[1]+this.sides[2])/2;
        return Math.sqrt(p*(p-this.sides[0])*(p-this.sides[1])*(p-this.sides[2]));
    }
}
