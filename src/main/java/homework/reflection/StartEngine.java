package homework.reflection;

public class StartEngine {

    @BeforeSuite
    void keyInTheLock() {
        System.out.println("Key in the ignition lock");
    }
    @AfterSuite
    void keyInFirstPosition() {
        System.out.println("Key in first position");
    }

    @Test(order = 1)
    void startIgnition() {
        System.out.println("1. Start ignition");
    }
    @Test(order = 2)
    void startFuelPump() {
        System.out.println("2. Start fuel pump");
    }
    @Test(order = 3)
    void startStarter() {
        System.out.println("3. Run Starter");
    }
    @Test(order = 4)
    void startEngine() {
        System.out.println("4. Start Engine");
    }
}
