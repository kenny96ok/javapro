package homework.generics.box;

import homework.generics.fruits.Fruit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Box<T extends Fruit> {
    private final List<T> fruitsInBox = new ArrayList<>();

    public void add(T fruit) {
        fruitsInBox.add(fruit);
    }

    public void addAll(T[] fruits) {
        fruitsInBox.addAll(Arrays.stream(fruits).toList());
    }

    public float getWeight() {
        return fruitsInBox.isEmpty() ? 0 : fruitsInBox.get(0).getWeight() * fruitsInBox.size();
    }

    public boolean compare(Box<? extends Fruit> another) {
        return this.getWeight() == another.getWeight();
    }

    private List<T> removeAllFruits() {
        List<T> list = this.fruitsInBox.stream().toList();
        fruitsInBox.clear();
        return list;
    }

    public void merge(Box<T> another) {
        fruitsInBox.addAll(another.removeAllFruits());
    }
}
