package homework.forex.calculator;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

class ResourceLoader implements Loader<List<InputStream>> {
    public List<InputStream> load() {
        List<InputStream> files = new ArrayList<>();
        try {
            CodeSource src = getClass().getProtectionDomain().getCodeSource();
            if (src == null) throw new RuntimeException("Data files were not found!");
            URL jar = src.getLocation();
            ZipInputStream zip = new ZipInputStream(jar.openStream());
            for (ZipEntry ze; (ze = zip.getNextEntry()) != null;) {
                if (ze.getName().endsWith(".txt")) {
                    files.add(getClass().getClassLoader().getResourceAsStream(ze.getName()));
                }
            }
        } catch (IOException ex) {
            throw new RuntimeException("Data files were not found!");
        }
        return files;
    }
}
