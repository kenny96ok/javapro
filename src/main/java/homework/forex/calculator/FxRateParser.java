package homework.forex.calculator;

import java.time.LocalDate;
import java.util.Currency;
import java.util.List;

public class FxRateParser implements Parser<List<FxRate>, FxRate> {
    private final Currency target;
    private LocalDate from;
    private LocalDate to;

    public FxRateParser(Currency target) {
        this.target = target;
        this.from = LocalDate.MIN;
        this.to = LocalDate.MAX;
    }

    public FxRateParser(Currency target, LocalDate from, LocalDate to) {
        this.target = target;
        this.from = from;
        this.to = to;
    }

    @Override
    public FxRate parse(List<FxRate> rates) {
        double sum = 0;
        List<FxRate> fxRates = rates.stream()
                .filter(fxRate -> fxRate.foreignCurrency().equals(target)).toList();
        if (fxRates.size() == 0) throw new RuntimeException("No information on this currency!");
        for (FxRate fxRate : fxRates) {
            sum += Double.parseDouble(fxRate.nationalCurrencyValue());
        }
        return new FxRate(fxRates.get(0).dateTime(), fxRates.get(0).foreignCurrency(),
                fxRates.get(0).unit(), String.valueOf(sum / fxRates.size()));
    }

    public FxRate parseInTimePeriod(List<FxRate> rates) {
        double sum = 0;
        List<FxRate> fxRates = rates.stream()
                .filter(fxRate -> fxRate.foreignCurrency().equals(target) &&
                        from.isBefore(fxRate.dateTime().toLocalDate()) &&
                        to.isAfter(fxRate.dateTime().toLocalDate()))
                .toList();
        if (fxRates.size() == 0) throw new RuntimeException("There is no information for this currency or for this period!");
        for (FxRate fxRate : fxRates) {
            sum += Double.parseDouble(fxRate.nationalCurrencyValue());
        }
        return new FxRate(fxRates.get(0).dateTime(), fxRates.get(0).foreignCurrency(),
                fxRates.get(0).unit(), String.valueOf(sum / fxRates.size()));
    }
}
