package homework.file.navigator;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FileNavigatorTest {
    FileNavigator fileNavigator = new FileNavigator();

    FileNavigatorTest() {
        File dirFiles = new File("src/main/java/homework/animal");
        for (File file : dirFiles.listFiles()) {
            fileNavigator.add(dirFiles.getPath(), new FileData(file.getName(), file.length(), file.getPath()));
        }
        dirFiles = new File("src/main/java/homework/abstraction");
        for (File file : dirFiles.listFiles()) {
            fileNavigator.add(dirFiles.getPath(), new FileData(file.getName(), file.length(), file.getPath()));
        }
    }

    @Test
    void shouldReturnListFileDataByPath() {
        List<FileData> fileData = fileNavigator.find("src/main/java/homework/animal");
        File dirFiles = new File("src/main/java/homework/animal");
        assertEquals(fileData.size(),dirFiles.listFiles().length);
        for (int i = 0; i < fileData.size(); i++) {
            assertEquals(dirFiles.listFiles()[i].getName(), fileData.get(i).getName());
        }
    }

    @Test
    void shouldReturnFilteredListFileDataByFileSize() {
        fileNavigator.filterBySize(500)
                .forEach(file -> assertTrue(file.getSizeInBytes()<500));
    }

    @Test
    void shouldRemoveFilesByPath() {
        assertNotNull(fileNavigator.find("src/main/java/homework/animal"));
        fileNavigator.remove("src/main/java/homework/animal");
        assertNull(fileNavigator.find("src/main/java/homework/animal"));
    }

    @Test
    void shouldReturnSortedListFileDataByFileSize() {
        List<FileData> sorted = fileNavigator.sortBySize();
        for (int i = 1; i < sorted.size(); i++) {
            assertTrue(sorted.get(i-1).getSizeInBytes()<sorted.get(i).getSizeInBytes());
        }
    }
}