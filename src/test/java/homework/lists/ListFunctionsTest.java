package homework.lists;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;
import java.util.stream.Stream;

import static homework.lists.ListFunctions.*;
import static org.junit.jupiter.api.Assertions.*;

class ListFunctionsTest {
    private List<String> words = new ArrayList<>(Arrays.stream(new String[]{
            "dog", "cat", "bird", "fish", "dog", "cat", "bird",
            "fish", "dog", "cat", "bird", "fish", "robot", "human"}).toList());
    private LinkedList<Integer> first = new LinkedList<>(), second = new LinkedList<>();

    @Test
    void shouldReturnCountOfOccurencyString() {
        assertEquals(3, countOccurance(words, "fish"));
    }

    @Test
    void shouldReturnListIntegers() {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        List<Integer> list = toList(array);
        for (int i = 0; i < array.length; i++) {
            assertEquals(array[i], list.get(i));
        }
    }

    @ParameterizedTest
    @MethodSource("arrayIntegerInputValues")
    void shouldReturnUniqueListIntegers(int[] array, int size) {
        List<Integer> unique = findUnique(toList(array));
        assertEquals(size, unique.size());
    }

    static Stream<Arguments> arrayIntegerInputValues() {
        return Stream.of(
                Arguments.of(new int[]{1, 2, 3, 1, 2, 1, 4, 5, 6, 6}, 3),
                Arguments.of(new int[]{1, 2, 3, 4}, 4),
                Arguments.of(new int[]{1, 2, 3, 4, 5, 1, 7}, 5),
                Arguments.of(new int[]{1, 2, 3, 4, 1, 2, 3, 4, 5, 6, 7, 8, 9}, 5)
        );
    }

    @Test
    void shouldPrintInConsoleCalculatedOccurance() {
        calcOccurance(words);
    }

    @ParameterizedTest
    @MethodSource("wordsAndCountsValues")
    void shouldReturnListCountsOfWord(String word, int count) {
        List<CountOfWord> result = findOccurance(words);
        for (CountOfWord countOfWord : result) {
            if (countOfWord.word().equals(word))
                assertEquals(countOfWord.count(), count);
        }

    }

    static Stream<Arguments> wordsAndCountsValues() {
        return Stream.of(
                Arguments.of("robot", 1),
                Arguments.of("cat", 3),
                Arguments.of("bird", 3),
                Arguments.of("fish", 3),
                Arguments.of("dog", 3),
                Arguments.of("human", 1)
        );
    }

    @Test
    void shouldAddStringInTheEndOfList() {
        LinkedList<String> wordsInLL = new LinkedList<>(words);
        addFirst(wordsInLL, "dolphin");
        assertEquals("dolphin", wordsInLL.getLast());
    }

    @Test
    void shouldAddStringInTheHeadOfList() {
        LinkedList<String> wordsInLL = new LinkedList<>(words);
        addLast(wordsInLL, "shark");
        assertEquals("shark", wordsInLL.getFirst());
    }

    @Test
    void shouldReturnJoinsTwoListIntegers() {
        for (int i = 0; i < 3; i++) {
            first.add(i + 1);
            second.add(i + 5);
        }
        LinkedList<Integer> test = join(first, second);
        assertEquals(6, test.size());
        assertEquals(1, test.getFirst());
        assertEquals(7, test.getLast());
        assertEquals(5, test.get(3));
    }

    @Test
    void shouldPrintInConsoleShuffledListStrings() {
        words.forEach(word -> System.out.print(word + " "));
        System.out.println();
        shuffle(new LinkedList<>(words)).forEach(word -> System.out.print(word + " "));
    }

    @Test
    void shouldReturnListIntersects() {
        for (int i = 0; i < 3; i++) {
            first.add(i + 1);
        }
        for (int i = 0; i < 5; i++) {
            second.add(i + 1);
        }
        LinkedList<Integer> intersect = intersect(first, second);
        assertEquals(3, intersect.size());
        for (int i = 0; i < 3; i++) {
            assertEquals(i + 1, intersect.get(i));
        }
    }
}