package homework.generics.box;

import homework.generics.fruits.Apple;
import homework.generics.fruits.Orange;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoxTest {
    private final Box<Apple> apples = new Box<>();
    private final Box<Apple> apples2 = new Box<>();
    private final Box<Orange> oranges = new Box<>();

    BoxTest() {
        for (int i = 0; i < 5; i++) {
            apples.add(new Apple());
            apples2.add(new Apple());
        }
        for (int i = 0; i < 3; i++) {
            oranges.add(new Orange());
        }
    }

    @Test
    void shouldReturnWeightOfBox() {
        assertEquals(5f, apples.getWeight());
        assertEquals(4.5f, oranges.getWeight());
    }

    @Test
    void shouldCompareDifferentBoxes() {
        assertTrue(apples.compare(apples2));
        assertFalse(apples.compare(oranges));
    }

    @Test
    void shouldMergeOneToTheOtherBox() {
        apples.merge(apples2);
        assertEquals(10.0f, apples.getWeight());
        assertEquals(0, apples2.getWeight());
    }
}