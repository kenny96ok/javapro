package homework.exceptions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayValueCalculatorTest {
    private String[][] numbers = {
            {"1", "2", "3", "4"},
            {"5", "6", "7", "8"},
            {"9", "10", "11", "12"},
            {"13", "14", "15", "16"}
    };

    @Test
    void shouldThrowMyArraySizeException_whenArrayRowsNotEqual4() {
        numbers = new String[][]{
                numbers[0],
                numbers[1],
                numbers[2],
                numbers[3],
                {"17", "18", "19", "20"}
        };
        assertThrowsExactly(MyArraySizeException.class, () -> ArrayValueCalculator.doCalc(numbers));
    }

    @Test
    void shouldThrowMyArraySizeException_whenArrayColsNotEqual4() {
        numbers = new String[][]{
                {"1", "2", "3", "4", "5"},
                numbers[1],
                numbers[2],
                numbers[3]
        };
        assertThrowsExactly(MyArraySizeException.class, () -> ArrayValueCalculator.doCalc(numbers));
    }

    @Test
    void shouldNotThrowMyArraySizeException() {
        assertDoesNotThrow(() -> ArrayValueCalculator.doCalc(numbers));
    }

    @Test
    void shouldThrowMyArrayDataException_whenNotNumberInCell() {
        numbers = new String[][]{
                {"1", "2", "3DVD", "4"},
                numbers[1],
                numbers[2],
                numbers[3]
        };
        assertThrowsExactly(MyArrayDataException.class, () -> ArrayValueCalculator.doCalc(numbers));
    }

    @Test
    void shouldReturnSumOfNumbers() {
        assertEquals(136, ArrayValueCalculator.doCalc(numbers));
    }

}